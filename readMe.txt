Клонируем проект командой:
git clone git@bitbucket.org:VZerg/testwork.git

Создаем вирутальное окружение в той же директории командой:
virtualenv -p python3 venv

Активируем виртуальное окружение:
source venv/bin/activate

для установки сопутствующий пакетов выполняем:
pip install -r requirements/generic.txt

для разработки:
pip install -r requirements/dev.txt

для продакшен окружения:
pip install -r requirements/prod.txt

в директории src/project создаем local_settings.py со следующими настройками:

from os.path import dirname, abspath

BASE_DIR = dirname(dirname(dirname(abspath(__file__))))

#Заполнить самостоятельно
SECRET_KEY = 'test-secret-key'

#Оставить нужное в нужном порядке
STRING_FORMAT = "%a %A %B %b %D %f %h %H %k %l %m %p %P %q %r %R %s %t %T %u %U %v %V %X %I %O"

#Путь к access логам apache
LOG_PATH = "/var/log/apache2/access.log"

PARSER_TEMP_FILE = "/tmp/PARSER.TEMP"

Выполнить миграцию:
python manage.py migrate

Добавить в crontab строку вида:
*/15 * * * * root /path/to/your/venv/python /path/to/your/manage.py parse_access_logs

