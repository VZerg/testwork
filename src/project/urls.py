# encoding: utf-8

from __future__ import unicode_literals

from django.conf.urls import url, include
from django.contrib import admin
from django.conf import settings


urlpatterns = [
    url(r'^api/', include('parse_api.urls')),
    url(r'^admin/', admin.site.urls),
    url(r'^', include('web_face.urls')),
]

if settings.DEBUG:
    from django.conf.urls.static import static
    from django.contrib.staticfiles.urls import staticfiles_urlpatterns
    import debug_toolbar

    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
    urlpatterns += staticfiles_urlpatterns()

    urlpatterns += [url(r'^__debug__/', include(debug_toolbar.urls)), ]
