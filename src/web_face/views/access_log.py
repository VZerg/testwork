# encoding: utf-8

from __future__ import unicode_literals

from django.contrib.auth.mixins import LoginRequiredMixin
from django_filters.views import FilterView

from core.filters import AccessLogFilter
from core.models import AccessLog

__all__ = ['AccessLogList', ]


class AccessLogList(LoginRequiredMixin, FilterView):
    filterset_class = AccessLogFilter
    module = AccessLog
    template_name = 'logs/list.html'
    context_object_name = 'logs'

    def get_queryset(self):
        return AccessLog.objects.all()
