# encoding: utf-8

from __future__ import unicode_literals

from django.conf.urls import url, include
from django.contrib.auth.views import LoginView, LogoutView

from django_filters.views import FilterView

from django.contrib.auth import views as auth_views
from web_face.views import AccessLogList


# Паттерны для аутентицикации
auth_patterns = [
    url(r'^login/$', LoginView.as_view(template_name='auth/login.html'), name='login'),
    url(r'^logout/$', LogoutView.as_view(template_name='auth/logout.html'), name='logout'),
]

log_patterns = [
    url(r'access_logs/$', AccessLogList.as_view(), name='logs_list'),
]

urlpatterns = [
    url(r'^', include(auth_patterns)),
    url(r'^', include(log_patterns))
]
