# encoding: utf-8

from __future__ import unicode_literals

from rest_framework import serializers

from drf_dynamic_fields import DynamicFieldsMixin

from core.models import AccessLog

__all__ = ['AccessLogSerializer']


class AccessLogSerializer(DynamicFieldsMixin, serializers.ModelSerializer):

    class Meta:
        model = AccessLog
        fields = (
            'remote_ip', 'local_ip', 'size_of_response', 'time_us', 'filename', 'remote_host',
            'protocol', 'num_keepalives', 'remote_logname', 'method', 'server_port', 'pid',
            'query_string', 'request_first_line', 'handler', 'status', 'time_received',
            'time_s', 'remote_user', 'url_path', 'server_name', 'server_name2', 'conn_status',
            'bytes_rx', 'bytes_tx',
            )
