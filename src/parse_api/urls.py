from django.contrib.auth import views as auto_views
from django.conf.urls import url

from rest_framework.routers import DefaultRouter

from parse_api.views import AccessLogViewSet


router = DefaultRouter(trailing_slash=False)

router.register(r'access_log', AccessLogViewSet)

urlpatterns = router.urls
