# encoding: utf-8

from __future__ import unicode_literals

from django_filters.rest_framework import DjangoFilterBackend
from rest_framework.viewsets import ViewSet, ReadOnlyModelViewSet

from core.filters import AccessLogFilter
from core.models import AccessLog
from parse_api.serializers import AccessLogSerializer


class AccessLogViewSet(ReadOnlyModelViewSet, ViewSet):
    serializer_class = AccessLogSerializer
    queryset = AccessLog.objects.all()
    filter_backends = (DjangoFilterBackend, )
    filter_class = AccessLogFilter

