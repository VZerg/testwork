# encoding: utf-8

from __future__ import unicode_literals

import os.path
import apache_log_parser
import pickle

from django.conf import settings

from .models import AccessLog, MODEL_FIELD_MAP


def parse():
    line_parse = apache_log_parser.make_parser(settings.STRING_FORMAT)

    last_position = 0

    if os.path.isfile(settings.PARSER_TEMP_FILE):
        with open(settings.PARSER_TEMP_FILE, 'rb') as f:
            last_position = pickle.load(f)

    with open(settings.LOG_PATH, 'r') as log_file:
        log_file.seek(last_position)
        for line in log_file:
            parsed_data = line_parse(line)
            save_logs(parsed_data)
        last_position = log_file.tell()

    with open(settings.PARSER_TEMP_FILE, 'w') as f:
        pickle.dump(last_position, f)


def save_logs(apache_logs):
    log_model = AccessLog()
    for key in MODEL_FIELD_MAP:
        setattr(log_model, MODEL_FIELD_MAP.get(key), apache_logs.get(key))
    log_model.save()

