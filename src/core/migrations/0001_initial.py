# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2017-07-07 09:07
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='AccessLog',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('remote_ip', models.CharField(blank=True, max_length=45, null=True, verbose_name='\u0423\u0434\u0430\u043b\u0435\u043d\u044b\u0439 IP')),
                ('local_ip', models.CharField(blank=True, max_length=45, null=True, verbose_name='\u041b\u043e\u043a\u0430\u043b\u044c\u043d\u044b\u0439 IP')),
                ('size_of_response', models.IntegerField(blank=True, null=True, verbose_name='\u0420\u0430\u0437\u043c\u0435\u0440 \u043e\u0442\u0432\u0435\u0442\u0430 \u043e\u0442 \u0441\u0435\u0440\u0432\u0435\u0440\u0430')),
                ('time_us', models.IntegerField(blank=True, null=True, verbose_name='\u0412\u0440\u0435\u043c\u044f \u043e\u0442\u0432\u0435\u0442\u0430 \u0441\u0435\u0440\u0432\u0435\u0440\u0430 microseconds')),
                ('filename', models.TextField(blank=True, null=True, verbose_name='\u0418\u043c\u044f \u0444\u0430\u0439\u043b\u0430')),
                ('remote_host', models.CharField(blank=True, max_length=255, null=True, verbose_name='\u0423\u0434\u0430\u043b\u0435\u043d\u043d\u044b\u0439 \u0445\u043e\u0441\u0442')),
                ('protocol', models.CharField(blank=True, max_length=10, null=True, verbose_name='\u041f\u0440\u043e\u0442\u043e\u043a\u043e\u043b \u0437\u0430\u043f\u0440\u043e\u0441\u0430')),
                ('num_keepalives', models.IntegerField(blank=True, null=True, verbose_name='\u041d\u043e\u043c\u0435\u0440 \u0437\u0430\u043f\u0440\u043e\u0441\u0430 \u0432 \u0443\u0441\u0442\u0430\u043d\u043e\u0432\u043b\u0435\u043d\u043d\u043e\u043c \u0441\u043e\u0435\u0434\u0438\u043d\u0435\u043d\u0438\u0438')),
                ('remote_logname', models.CharField(blank=True, max_length=255, null=True, verbose_name='\u0423\u0434\u0430\u043b\u0435\u043d\u043d\u044b\u0439 \u043b\u043e\u0433\u0438\u043d')),
                ('method', models.CharField(blank=True, max_length=10, null=True, verbose_name='\u041c\u0435\u0442\u043e\u0434 \u0437\u0430\u043f\u0440\u043e\u0441\u0430')),
                ('server_port', models.IntegerField(blank=True, null=True, verbose_name='\u041f\u043e\u0440\u0442')),
                ('pid', models.IntegerField(blank=True, verbose_name='Id \u043f\u0440\u043e\u0446\u0435\u0441\u0441\u0430\u043e\u0431\u0441\u043b\u0443\u0436\u0438\u0432\u0430\u0432\u0448\u0435\u0433\u043e \u0437\u0430\u043f\u0440\u043e\u0441')),
                ('query_string', models.TextField(blank=True, null=True, verbose_name='\u0421\u0442\u0440\u043e\u043a\u0430 \u0437\u0430\u043f\u0440\u043e\u0441\u0430 \u0441 \u0432\u043e\u043f\u0440\u043e\u0441\u0438\u043b\u0442\u0435\u043b\u044c\u043d\u044b\u043c \u0437\u043d\u0430\u043a\u043e\u043c')),
                ('request_first_line', models.TextField(blank=True, null=True, verbose_name='\u041f\u0435\u0440\u0432\u0430\u044f \u043b\u0438\u043d\u0438\u044f \u0437\u0430\u043f\u0440\u043e\u0441\u0430')),
                ('handler', models.TextField(blank=True, null=True, verbose_name='\u041e\u0431\u0440\u0430\u0431\u043e\u0442\u0438\u043a \u043e\u0442\u0432\u0435\u0442\u0430')),
                ('status', models.IntegerField(blank=True, null=True, verbose_name='\u0421\u0442\u0430\u0442\u0443\u0441 \u0437\u0430\u043f\u0440\u043e\u0441\u0430')),
                ('time_received', models.DateTimeField(blank=True, null=True, verbose_name='\u0412\u0440\u0435\u043c\u044f \u0437\u0430\u043f\u0440\u043e\u0441\u0430')),
                ('time_s', models.IntegerField(blank=True, null=True, verbose_name='\u0412\u0440\u0435\u043c\u044f \u0433\u0435\u043d\u0438\u0440\u0430\u0446\u0438\u0438 \u043e\u0442\u0432\u0435\u0442\u0430')),
                ('remote_user', models.TextField(blank=True, null=True, verbose_name='\u0423\u0434\u0430\u043b\u0435\u043d\u043d\u044b\u0439 \u043f\u043e\u043b\u044c\u0437\u043e\u0432\u0430\u0442\u0435\u043b\u044c')),
                ('url_path', models.TextField(blank=True, null=True, verbose_name='\u041f\u0443\u0442\u044c')),
                ('server_name', models.CharField(blank=True, max_length=20, null=True, verbose_name='\u0421\u0435\u0440\u0432\u0435\u0440 \u043e\u0431\u0441\u043b\u0443\u0436\u0438\u0432\u0430\u044e\u0449\u0438\u0439 \u0437\u0430\u043f\u0440\u043e\u0441')),
                ('server_name2', models.CharField(blank=True, max_length=20, null=True, verbose_name='\u0418\u043c\u044f \u0441\u0435\u0440\u0432\u0435\u0440\u0430 \u0432 \u0441\u043e\u043e\u0442\u0432\u0435\u0442\u0441\u0432\u0438\u0438 \u0441 \u043d\u0430\u0441\u0442\u0440\u043e\u0439\u043a\u0430\u043c\u0438')),
                ('conn_status', models.CharField(blank=True, max_length=2, null=True, verbose_name='\u0421\u0442\u0430\u0442\u0443\u0441 \u043f\u043e\u0434\u043a\u043b\u044e\u0447\u0435\u043d\u0438\u044f')),
                ('bytes_rx', models.IntegerField(blank=True, null=True, verbose_name='\u041f\u043e\u043b\u0443\u0447\u0435\u043d\u043d\u044b\u0439 \u043e\u0431\u044a\u0435\u043c')),
                ('bytes_tx', models.IntegerField(blank=True, null=True, verbose_name='\u041e\u0442\u043f\u0440\u0430\u0432\u0435\u043d\u043d\u044b\u0439 \u043e\u0431\u044a\u0435\u043c')),
            ],
            options={
                'verbose_name': '\u0417\u0430\u043f\u0440\u043e\u0441',
                'verbose_name_plural': '\u0417\u0430\u043f\u0440\u043e\u0441\u044b',
            },
        ),
    ]
