# encoding: utf-8

from __future__ import unicode_literals

from django_filters.rest_framework import FilterSet

from core.models import AccessLog

__all__ = ['AccessLogFilter', ]


class AccessLogFilter(FilterSet):

    class Meta:
        model = AccessLog
        fields = {
            'remote_ip': ('exact', ),
            'local_ip':('exact', ),
            'time_received': ('gte', 'lte'),
        }
