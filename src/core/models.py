# encoding: utf-8

from __future__ import unicode_literals

from django.db import models
from django.utils.encoding import python_2_unicode_compatible

MODEL_FIELD_MAP = {
    'response_bytes': 'size_of_response',
    'response_bytes_clf': 'size_of_response',
    'remote_ip': 'remote_ip',
    'local_ip': 'local_ip',
    'time_us': 'time_us',
    'filename': 'filename',
    'remote_host': 'remote_host',
    'protocol': 'protocol',
    'num_keepalives': 'num_keepalives',
    'remote_logname': 'remote_logname',
    'method': 'method',
    'server_port': 'server_port',
    'pid': 'pid',
    'query_string': 'query_string',
    'request_first_line': 'request_first_line',
    'handler': 'handler',
    'status': 'status',
    'time_received_tz_datetimeobj': 'time_received',
    'time_s': 'time_s',
    'remote_user': 'remote_user',
    'url_path': 'url_path',
    'server_name': 'server_name',
    'server_name2': 'server_name2',
    'conn_status': 'conn_status',
    'bytes_rx': 'bytes_rx',
    'bytes_tx': 'bytes_tx',
}

@python_2_unicode_compatible
class AccessLog(models.Model):
    remote_ip = models.CharField(
        max_length=45,
        blank=True,
        null=True,
        verbose_name='Удаленый IP',
    )
    local_ip = models.CharField(
        max_length=45,
        blank=True,
        null=True,
        verbose_name='Локальный IP',
    )
    size_of_response = models.CharField(
        max_length=20,
        blank=True,
        null=True,
        verbose_name='Размер ответа от сервера',
    )
    time_us = models.CharField(
        max_length=10,
        blank=True,
        null=True,
        verbose_name='Время ответа сервера microseconds',
    )
    filename = models.TextField(
        blank=True,
        null=True,
        verbose_name='Имя файла'
    )
    remote_host = models.CharField(
        max_length=255,
        blank=True,
        null=True,
        verbose_name='Удаленный хост',
    )
    protocol = models.CharField(
        max_length=10,
        blank=True,
        null=True,
        verbose_name='Протокол запроса',
    )
    num_keepalives = models.CharField(
        max_length=10,
        blank=True,
        null=True,
        verbose_name='Номер запроса в установленном соединении',
    )
    remote_logname = models.CharField(
        max_length=255,
        blank=True,
        null=True,
        verbose_name='Удаленный логин',
    )
    method = models.CharField(
        max_length=10,
        blank=True,
        null=True,
        verbose_name='Метод запроса',
    )
    server_port = models.CharField(
        max_length=10,
        blank=True,
        null=True,
        verbose_name='Порт',
    )
    pid = models.CharField(
        max_length=10,
        blank=True,
        null=True,
        verbose_name='Id процесса обслуживавшего запрос',
    )
    query_string = models.TextField(
        blank=True,
        null=True,
        verbose_name='Строка запроса с вопросилтельным знаком',
    )
    request_first_line = models.TextField(
        blank=True,
        null=True,
        verbose_name='Первая линия запроса',
    )
    handler = models.TextField(
        blank=True,
        null=True,
        verbose_name='Обработчик ответа',
    )
    status = models.CharField(
        max_length=3,
        blank=True,
        null=True,
        verbose_name='Статус запроса',
    )
    time_received = models.DateTimeField(
        blank=True,
        null=True,
        verbose_name='Время запроса',
    )
    time_s = models.CharField(
        max_length=10,
        blank=True,
        null=True,
        verbose_name='Время генирации ответа',
    )
    remote_user = models.TextField(
        blank=True,
        null=True,
        verbose_name='Удаленный пользователь',
    )
    url_path = models.TextField(
        blank=True,
        null=True,
        verbose_name='Путь',
    )
    server_name = models.CharField(
        max_length=20,
        blank=True,
        null=True,
        verbose_name='Сервер обслуживающий запрос',
    )
    server_name2 = models.CharField(
        max_length=20,
        blank=True,
        null=True,
        verbose_name='Имя сервера в соответсвии с настройками',
    )
    conn_status = models.CharField(
        max_length=2,
        blank=True,
        null=True,
        verbose_name='Статус подключения',
    )
    bytes_rx = models.CharField(
        max_length=10,
        blank=True,
        null=True,
        verbose_name='Полученный объем',
    )
    bytes_tx = models.CharField(
        max_length=10,
        blank=True,
        null=True,
        verbose_name='Отправенный объем',
    )

    def __str__(self):
        return 'Запрос к серверу №{}'.format(self.id)

    class Meta:
        verbose_name = 'Запрос'
        verbose_name_plural = 'Запросы'
