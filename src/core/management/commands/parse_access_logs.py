# encoding: utf-8

from __future__ import unicode_literals

from django.core.management.base import BaseCommand
from core.parser import parse
from datetime import datetime
import logging

logger = logging.getLogger('parser')


class Command(BaseCommand):
    help = 'Сохраняет access логи apache в базу данных'

    def handle(self, *args, **options):
        try:
            parse()
        except Exception as e:
            logger.error("{}: {}".format(datetime.now(), e))
