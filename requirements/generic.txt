Django==1.11
djangorestframework==3.6.3
apache-log-parser==1.7.0
django-filter==1.0.4
drf-dynamic-fields==0.2.0
